using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    public enum CharacterAttackType{ Melee, Ranged }
    public CharacterAttackType myAttackType;

    public GameObject targetedEnemy;
    public float attackRange, exitAttackRange;
    public float attackRotateSpeed;
    
    [SerializeField]
    public Animator anim;
    public PlayerController pController;
    private PlayerMovement moveScript;

    public bool basicAttackIdle = false;
    public bool isHeroAlive;
    public bool performMeleeAttack = true;    

    void Start()
    {
        InitializeComponents();
    }

    private void InitializeComponents()
    {
        moveScript = GetComponent<PlayerMovement>();
        pController = GetComponent<PlayerController>();
    }

    void Update()
    {
        if(targetedEnemy != null)
        {
            if (targetedEnemy.GetComponent<Collider>().isTrigger)
            {
                targetedEnemy = targetedEnemy.transform.parent.gameObject;
                
                if (targetedEnemy.CompareTag("PlayerB"))
                { targetedEnemy = targetedEnemy.transform.parent.gameObject; }               
            }
            if (Vector3.Distance(gameObject.transform.position, targetedEnemy.transform.position) > attackRange && Vector3.Distance(gameObject.transform.position, targetedEnemy.transform.position) < exitAttackRange)
            {
                moveScript.agent.SetDestination(targetedEnemy.transform.position);
                moveScript.agent.stoppingDistance = attackRange;

                Quaternion rotationLookAt = Quaternion.LookRotation(targetedEnemy.transform.position - transform.position);
                float rotationY = Mathf.SmoothDampAngle(transform.eulerAngles.y, rotationLookAt.eulerAngles.y, ref moveScript.rotateSpeedMovement, attackRotateSpeed * (Time.deltaTime * 5));
            }
            else
            {
                if(myAttackType == CharacterAttackType.Melee)
                {
                    if (performMeleeAttack) { StartCoroutine(MeleeAttack()); }
                }
                
            }
        }
        else if(targetedEnemy == null)
        {
            
            { anim.SetBool("Basic Attack", false); performMeleeAttack = true; }
        }
    }

    IEnumerator MeleeAttack()
    {
        performMeleeAttack = false;
        anim.SetBool("Basic Attack", true);

        yield return new WaitForSecondsRealtime(pController.attackDelay / ((100 + pController.attackDelay) * 0.01f));

        if(targetedEnemy == null)
        { anim.SetBool("Basic Attack", false); performMeleeAttack = true; }
    }

    public void MeleeDoDamage()
    {
        if (targetedEnemy != null)
        {
            if (targetedEnemy.GetComponent<Targeteable>().myType == Targeteable.EnemyType.Minion)
            { targetedEnemy.GetComponent<MinionController>().TakeDamage(pController.AttackDamage, gameObject); }            
            if (targetedEnemy.GetComponent<Targeteable>().myType == Targeteable.EnemyType.Tower)
            { targetedEnemy.GetComponent<TowerController>().TakeDamage(pController.AttackDamage, gameObject); }
            if (targetedEnemy.GetComponent<Targeteable>().myType == Targeteable.EnemyType.Golem)
            { targetedEnemy.GetComponent<Golem>().TakeDamage(pController.AttackDamage, gameObject); }
            if (targetedEnemy.GetComponent<Targeteable>().myType == Targeteable.EnemyType.Wolf)
            { targetedEnemy.GetComponent<Wolfes>().TakeDamage(pController.AttackDamage, gameObject); }
            if (targetedEnemy.GetComponent<Targeteable>().myType == Targeteable.EnemyType.Nexus)
            { targetedEnemy.GetComponent<NexusController>().TakeDamage(pController.AttackDamage); }
        }
        performMeleeAttack = true;
    }
}
