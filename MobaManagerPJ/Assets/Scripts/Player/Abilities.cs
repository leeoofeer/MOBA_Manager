using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Abilities : MonoBehaviour
{
    public Animator anim;
    PlayerController myPC;
    public GameObject Sk1, Sk2, Sk3;
    public GameObject InactiveMana1, InactiveMana2, InactiveMana3;


    [Header("Skill 1")]
    public Image abilityImageOne;
    public float cooldownOne = 5;
    bool isCooldownOne = false;
    public KeyCode skill1;
    float sk1Consumption = 100f;


    Vector3 position;
    public Canvas abilityOneCanvas;
    public Image skillshot;
    public Transform player;


    [Header("Skill 2")]
    public Image abilityImageTwo;
    public float cooldownTwo = 5;
    bool isCooldownTwo = false;
    public KeyCode skill2;
    float sk2Consumption = 250f;

    [SerializeField]
    GameObject bomb;
    public Image targetCircle;
    public Image indicatorRangeCircle;
    public Canvas abilityTwoCanvas;
    private Vector3 posUp;
    public float maxAbility2Distance;


    [Header("Skill 3")]
    public Image abilityImageThree;
    public float cooldownThree = 5;
    public bool isCooldownThree = false;
    public KeyCode skill3;
    public GameObject skillShotPrefab;
    public Transform skillShotStartPos;
    public bool canSkillshot = true;
    float sk3Consumption = 150f;


    void Start()
    {
        InitializeComponents();
        InitializeUI();       
    }

    private void InitializeUI()
    {
        abilityImageOne.fillAmount = 1;
        abilityImageTwo.fillAmount = 1;
        abilityImageThree.fillAmount = 1;
        if (myPC.currentLevel == 1)
        {
            Sk1.SetActive(true);
        }
        else if (myPC.currentLevel == 2)
        {
            Sk2.SetActive(true);

        }
        else if (myPC.currentLevel == 3)
        {
            Sk3.SetActive(true);
        }
    }
    private void InitializeComponents()
    {
        skillshot.GetComponent<Image>().enabled = false;
        targetCircle.GetComponent<Image>().enabled = false;
        indicatorRangeCircle.GetComponent<Image>().enabled = false;
        myPC = GetComponent<PlayerController>();
    }

    void Update()
    {
        if (myPC.currentLevel == 1)
        {
            SkillOne();
        }else if (myPC.currentLevel == 2)
        {
            SkillOne();
            SkillTwo();
        }
        else if (myPC.currentLevel == 3)
        {
            SkillOne();
            SkillTwo();
            SkillThree();
        }
        

        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        // Skillshot
        if(Physics.Raycast(ray,out hit, Mathf.Infinity))
        {
            position = new Vector3(hit.point.x, hit.point.y, hit.point.z);
        }

        Quaternion transRot = Quaternion.LookRotation(position - player.transform.position);
        abilityOneCanvas.transform.rotation = Quaternion.Lerp(transRot, abilityOneCanvas.transform.rotation, 0f);


        // SkillArea
        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            if(hit.collider.gameObject != this.gameObject)
            {
                posUp = new Vector3(hit.point.x, 10f, hit.point.z);
                position = hit.point;
            }
        }

        var hitPosDir = (hit.point - transform.position).normalized;
        float distance = Vector3.Distance(hit.point, transform.position);
        distance = Mathf.Min(distance, maxAbility2Distance);
        var newHitPos = transform.position + hitPosDir * distance;
        abilityTwoCanvas.transform.position = (newHitPos);
       



    }

    void SkillOne()
    {
        if (myPC.currentMana > sk1Consumption) { InactiveMana1.SetActive(false); }
        else { InactiveMana1.SetActive(true); }

        if (Input.GetKey(skill1) && isCooldownOne == false && myPC.currentMana >= sk1Consumption)
        {            
            isCooldownOne = true; abilityImageOne.fillAmount = 0;
            myPC.TakeDamage(-100);
            myPC.currentMana -= sk1Consumption; myPC.UpdateManaBar();
        }

        if (isCooldownOne)
        {
            abilityImageOne.fillAmount += 1 / cooldownOne * Time.deltaTime;

            if (abilityImageOne.fillAmount >= 1)
            { abilityImageOne.fillAmount = 1; isCooldownOne = false; }
        }
    }

    void SkillTwo()
    {
        if (myPC.currentMana > sk2Consumption) { InactiveMana2.SetActive(false); }
        else { InactiveMana2.SetActive(true); }
        if (Input.GetKey(skill2) && isCooldownTwo == false && myPC.currentMana >= sk2Consumption)
        {            
            indicatorRangeCircle.GetComponent<Image>().enabled = true;
            targetCircle.GetComponent<Image>().enabled = true;
            skillshot.GetComponent<Image>().enabled = false;            
        }

        if (targetCircle.GetComponent<Image>().enabled == true && Input.GetMouseButton(0) && myPC.currentMana >= sk2Consumption)
        {
            isCooldownTwo = true; abilityImageTwo.fillAmount = 0; myPC.currentMana -= sk2Consumption; myPC.UpdateManaBar();
            GameObject bombInstance = Instantiate(bomb, abilityTwoCanvas.transform.position, Quaternion.identity);
            bombInstance.GetComponent<BombController>().skillDamage = 100;

        }
        else if (targetCircle.GetComponent<Image>().enabled == true && Input.GetMouseButton(1))
        {
            targetCircle.GetComponent<Image>().enabled = false;
            indicatorRangeCircle.GetComponent<Image>().enabled = false;
        }

        if (isCooldownTwo)
        {
            abilityImageTwo.fillAmount += 1 / cooldownTwo * Time.deltaTime;
            indicatorRangeCircle.GetComponent<Image>().enabled = false;
            targetCircle.GetComponent<Image>().enabled = false;

            if (abilityImageTwo.fillAmount >= 1)
            { abilityImageTwo.fillAmount = 1; isCooldownTwo = false; }
        }
    }

    void SkillThree()
    {
        if (myPC.currentMana > sk3Consumption) { InactiveMana3.SetActive(false); }
        else { InactiveMana3.SetActive(true); }
        if (Input.GetKey(skill3) && isCooldownThree == false && myPC.currentMana >= sk3Consumption)
        {
            skillshot.GetComponent<Image>().enabled = true;
            indicatorRangeCircle.GetComponent<Image>().enabled = false;
            targetCircle.GetComponent<Image>().enabled = false;
        }

        if (skillshot.GetComponent<Image>().enabled == true && Input.GetMouseButton(0) && myPC.currentMana >= sk3Consumption)
        {
            isCooldownThree = true; abilityImageThree.fillAmount = 0;
            if (canSkillshot)
            {
                isCooldownThree = true;
                abilityImageThree.fillAmount = 0;
                myPC.currentMana -= sk3Consumption; myPC.UpdateManaBar();
                StartCoroutine(corMysticShot());
            }
        }
        else if (skillshot.GetComponent<Image>().enabled == true && Input.GetMouseButton(1))
        {
            skillshot.GetComponent<Image>().enabled = false;
          
        }

        if (isCooldownThree)
        {
            abilityImageThree.fillAmount += 1 / cooldownThree * Time.deltaTime;
            skillshot.GetComponent<Image>().enabled = false;

            if (abilityImageThree.fillAmount >= 1)
            { abilityImageThree.fillAmount = 1; isCooldownThree = false; }
        }
    }

    IEnumerator corMysticShot()
    {
        canSkillshot = false;
        anim.SetBool("Skillshot", true);

        yield return new WaitForSeconds(1.5f);

        anim.SetBool("Skillshot", false);
    }

    //Animation Event

    public void SpawnMysticShot()
    {
        canSkillshot = true;
        GameObject newbullet = Instantiate(skillShotPrefab, new Vector3(skillShotStartPos.transform.position.x, 
                                                                        skillShotStartPos.transform.position.y+0.5f, 
                                                                        skillShotStartPos.transform.position.z), skillShotStartPos.transform.rotation);
        newbullet.layer = 8;
        BulletMovement bm = newbullet.GetComponent<BulletMovement>();
        bm.movementSpeed = 5;
    }
}
