using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectTarget : MonoBehaviour
{
    [SerializeField]
    public GameObject selectedHero;
    public bool heroPlayer;
    RaycastHit hit;
    public LayerMask IgnoreMe;

    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity, ~IgnoreMe))
            {
                if (hit.collider.GetComponent<Targeteable>() != null)
                {
                    if(hit.collider.gameObject.GetComponent<Targeteable>().myType == Targeteable.EnemyType.Minion)
                    {
                        selectedHero.GetComponent<PlayerAttack>().targetedEnemy = hit.collider.gameObject;
                    }else if (hit.collider.gameObject.GetComponent<Targeteable>().myType == Targeteable.EnemyType.Tower)
                    {
                        selectedHero.GetComponent<PlayerAttack>().targetedEnemy = hit.collider.gameObject;
                    }else if (hit.collider.gameObject.GetComponent<Targeteable>().myType == Targeteable.EnemyType.Golem)
                    {
                        selectedHero.GetComponent<PlayerAttack>().targetedEnemy = hit.collider.gameObject;
                    }else if (hit.collider.gameObject.GetComponent<Targeteable>().myType == Targeteable.EnemyType.Wolf)
                    {
                        selectedHero.GetComponent<PlayerAttack>().targetedEnemy = hit.collider.gameObject;
                    }
                    else if (hit.collider.gameObject.GetComponent<Targeteable>().myType == Targeteable.EnemyType.Nexus)
                    {
                        selectedHero.GetComponent<PlayerAttack>().targetedEnemy = hit.collider.gameObject;
                    }
                }
                else if (hit.collider.GetComponent<Targeteable>() == null)
                {
                    selectedHero.GetComponent<PlayerAttack>().targetedEnemy = null;
                }
            }
        }
    }
}
