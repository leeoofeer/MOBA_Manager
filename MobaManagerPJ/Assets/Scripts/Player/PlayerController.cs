using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class PlayerController : MonoBehaviour
{
    private float attackDamage;
    public float AttackDamage
    {
        get { return attackDamage; }
        set { attackDamage = value; }
    }

    private int maxHealth;
    public int MaxHealth
    { get { return maxHealth; } }

    private int maxMana;
    public int MaxMana
    { get { return maxMana; } }

    public EventHandler OnObtainedGold;
    
    bool isAlive;
    int armor;
    float currentHealth, hpRegen;
    public float currentMana;
    float manaRegen;
    float currentXP, nextXP;
    public int currentLevel;
    int maxLevel;
    public float attackDelay;
    float gold;
    [SerializeField] Image healthBar;
    [SerializeField] Image manaBar;
    [SerializeField] Image xpBar;
    [SerializeField] TMP_Text xpBarText;
    [SerializeField] TMP_Text levelText;
    [SerializeField] TMP_Text healthBarText;
    [SerializeField] TMP_Text manaBarText;
    [SerializeField] TMP_Text coinsText;
    Abilities myAbs;
    PlayerAttack pAtkScript;

    [SerializeField] TMP_Text attackDamageText;
    [SerializeField] TMP_Text armorText;
    [SerializeField] TMP_Text healthRegenText;
    [SerializeField] TMP_Text minionCooldownText;
    [SerializeField] TMP_Text manaRegenText;


    [SerializeField] TMP_Text debugVelocityText;
    [SerializeField] TMP_Text debugAcelerationText;
    float debugVelocity, debugAceleration;
    public Rigidbody rb;

    void Start()
    {
        InitializeComponents();
        InitializePlayer();
        StartCoroutine(HealthRegen(hpRegen));
        StartCoroutine(ManaRegen(manaRegen));
        UpdateHealthBar();
        UpdateManaBar();
        UpdateXpBar();
        UpdateCoinCounter();
    }


    #region Increase Stats
    public void AddArmor(int amount)
    {
        armor += amount;
    }
    public void AddDamage(int amount)
    {
        attackDamage += amount;
    }
    public void AddMaxHealth(int amount)
    {
        maxHealth += amount;
        UpdateHealthBar();
    }
    public void AddMaxMana(int amount)
    {
        maxMana += amount;
        UpdateManaBar();
    }
    #endregion

    public void SumGold(int amount)
    {
        gold += amount;
        UpdateCoinCounter();
        OnObtainedGold?.Invoke(this, EventArgs.Empty);
    }    

    private void InitializeComponents()
    {
        rb = GetComponent<Rigidbody>();
        pAtkScript = GetComponent<PlayerAttack>();
        myAbs = GetComponent<Abilities>();
    }
    private void InitializePlayer()
    {
        isAlive = true;
        armor = 1;
        maxHealth = 1000; currentHealth = maxHealth; hpRegen = 1.667f;
        currentXP = 0; nextXP = 100; currentLevel = 1; maxLevel = 3;        
        maxMana = 1500; currentMana = maxMana; manaRegen = 9;
        attackDelay = 1f; attackDamage = 50f;
        gold = 1000;
        UpdateStatsPanel();
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F)) TakeDamage(10);
        if (Input.GetKeyDown(KeyCode.G)) GainXP(50);
        if (Input.GetKeyDown(KeyCode.F)) FillMana(100);

        debugVelocity = rb.velocity.magnitude;
        debugVelocityText.text = "Velocity: " + debugVelocity.ToString();
        debugAcelerationText.text = "Aceleration: " + debugAceleration.ToString();

    }

    #region Cheats
    public void GainXP(float exp)
    {
        currentXP += exp;
        UpdateXpBar();             
    }

    public void FillMana(float mana)
    {
        currentMana += mana;
        UpdateManaBar();
    }
    #endregion

    bool IsMaxLevel(int level)
    {
        if (level == maxLevel)
        {
            return true;
        }
        return false;
    }
   
    public bool GoldCheck(int amount, bool buying)
    {
        if (buying)
        {
            if (amount > gold)
            {
                return false;
            }
            else
            {
                gold -= amount;
                UpdateCoinCounter();
                OnObtainedGold?.Invoke(this, EventArgs.Empty);                
                return true;
            }
        }
        else
        {
            if (amount > gold)
            {
                return false;
            }
            else if (amount < gold)
            {
                return true;
            }else
            {
                return false;
            }
        }   
    }

    public void AddSkill(int level)
    {
        switch (level)
        {            
            case 2:
                myAbs.Sk2.SetActive(true);
                break;
            case 3:
                myAbs.Sk3.SetActive(true);
                break;
            default:
                break;
        }
    }

    public void TakeDamage(float damage)
    {
        currentHealth -= (damage - armor);
        UpdateHealthBar();
        if (currentHealth <= 0) { Die(); }
        rb.velocity = Vector3.zero;
    }
    IEnumerator HealthRegen(float hpRegen)
    {
        while (isAlive)
        {
            if (currentHealth > 0 )
            {
                currentHealth += hpRegen;
                UpdateHealthBar();               
            }
            yield return new WaitForSeconds(1f);
        }
    }
    IEnumerator ManaRegen(float manaRegen)
    {
        while (isAlive)
        {
            if (currentMana > 0)
            {
                currentMana += manaRegen;
                UpdateManaBar();
            }
            yield return new WaitForSeconds(1f);
        }
    }

    #region UpdateUIRelated
    private void UpdateHealthBar()
    {
        if (currentHealth > maxHealth) { currentHealth = maxHealth; }
        healthBar.fillAmount = (currentHealth / maxHealth );
        healthBarText.text = currentHealth.ToString("0") + "/" + maxHealth.ToString();
    }
    public void UpdateManaBar()
    {
        if (currentMana > maxMana) { currentMana = maxMana; }
        manaBar.fillAmount = (currentMana / maxMana);
        manaBarText.text = currentMana.ToString("0") + "/" + maxMana.ToString();
    }
    public void UpdateXpBar()
    {
        if (!IsMaxLevel(currentLevel))
        {
            if (currentXP >= nextXP)
            {
                currentXP = 0f;
                nextXP += 200f;
                currentLevel++;
                AddSkill(currentLevel);
            }

            if (IsMaxLevel(currentLevel))
            {
                currentXP = nextXP;
                xpBar.fillAmount = (currentXP / nextXP);
                xpBarText.text = currentXP.ToString("0") + "/" + nextXP.ToString();
                levelText.text = "Level: " + currentLevel.ToString("0");
            }
            else
            {
                xpBar.fillAmount = (currentXP / nextXP);
                xpBarText.text = currentXP.ToString("0") + "/" + nextXP.ToString();
                levelText.text = "Level: " + currentLevel.ToString("0");
            }
        }
        else
        {
            currentXP = nextXP;
            xpBar.fillAmount = (currentXP / nextXP);
            xpBarText.text = currentXP.ToString("0") + "/" + nextXP.ToString();
        }
    }
    public void UpdateCoinCounter()
    {
        if (gold < 0) gold = 0;
        coinsText.text = gold.ToString("0");
    }

    public void UpdateStatsPanel()
    {
        attackDamageText.text = attackDamage.ToString();
        armorText.text = armor.ToString();
        healthRegenText.text = hpRegen.ToString();
        minionCooldownText.text = GameObject.Find("NexusB").GetComponent<NexusController>().spawnRate.ToString();
        manaRegenText.text = manaRegen.ToString();
    }
    #endregion
    void Die()
    {
        pAtkScript.targetedEnemy = null; pAtkScript.performMeleeAttack = false;
        Destroy(gameObject);
    }
}
