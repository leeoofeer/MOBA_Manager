using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour
{
    public NavMeshAgent agent;
    public LayerMask IgnoreMe;
    PlayerController pc;

    public float rotateSpeedMovement = 0.1f;
    float rotateVelocity;

    private PlayerAttack myPlayerAttackScript;

    void Start()
    {
        pc = GetComponent<PlayerController>();
        agent = gameObject.GetComponent<NavMeshAgent>();
        myPlayerAttackScript = gameObject.GetComponent<PlayerAttack>();
    }

    void Update()
    {
        if(myPlayerAttackScript.targetedEnemy != null)
        {
            if (myPlayerAttackScript.targetedEnemy.GetComponent<PlayerAttack>() != null)
            {
                if (!myPlayerAttackScript.targetedEnemy.GetComponent<PlayerAttack>().isHeroAlive)
                {
                    myPlayerAttackScript.targetedEnemy = null;
                }
            }          
        }

        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit hit;
            
            if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity, ~IgnoreMe))
            {
                if (hit.collider.tag == "Floor")
                {
                    pc.rb.velocity = Vector3.zero;
                    agent.SetDestination(hit.point);
                    myPlayerAttackScript.targetedEnemy = null;
                    agent.stoppingDistance = 0;

                    Quaternion rotationToLookAt = Quaternion.LookRotation(hit.point - transform.position);
                    float rotationY = Mathf.SmoothDampAngle(transform.eulerAngles.y, rotationToLookAt.eulerAngles.y, ref rotateVelocity, rotateSpeedMovement * (Time.deltaTime * 5));

                    transform.eulerAngles = new Vector3(0, rotationY, 0);
                }
            }
               
        }
    }
}
