using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class PlayerAnimator : MonoBehaviour
{
    NavMeshAgent agent;
    Abilities myAbs;

    public Animator anim;
    float speed = 0;
    float motionSmoothTime = 0.1f;
    [SerializeField]
    public PlayerAttack pAtakScript;
    
    void Start()
    {
        agent = GetComponentInParent<NavMeshAgent>();
        myAbs = GetComponentInParent<Abilities>();
    }

    // Update is called once per frame
    void Update()
    {        
        speed = agent.velocity.magnitude / agent.speed;     
        anim.SetFloat("Speed", speed, motionSmoothTime, Time.deltaTime);
    }

    public void OnMeleeDamage()
    {
        pAtakScript.MeleeDoDamage();
    }
    public void SpawnMysticShot()
    {
        myAbs.SpawnMysticShot();
    }
}
