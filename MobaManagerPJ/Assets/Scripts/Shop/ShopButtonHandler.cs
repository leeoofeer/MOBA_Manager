using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopButtonHandler : MonoBehaviour
{
    [SerializeField]GameObject shopPanel;
    public event EventHandler OnOpenShop;


    public void ShopWindowHandler()
    {
        shopPanel.SetActive(!shopPanel.activeInHierarchy);
        OnOpenShop?.Invoke(this, EventArgs.Empty);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            shopPanel.SetActive(!shopPanel.activeInHierarchy);
            OnOpenShop?.Invoke(this, EventArgs.Empty);
        }
    }
}
