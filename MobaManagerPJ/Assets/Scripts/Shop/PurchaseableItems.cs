using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class PurchaseableItems : MonoBehaviour
{
    ShopButtonHandler SBH;
    PlayerController pcController;
    NexusController nxController;
    TowerController tower1, tower2;
    Button myButton;
    TMP_Text buttonText;
    int price = 0000;
    PlayerController pc;
    PurchaseableType pType;

    void Start()
    {
        InitializeComponents();        
    }

    private void InitializeComponents()
    {
        SBH = GameObject.Find("ShopButton").GetComponent<ShopButtonHandler>();
        SBH.OnOpenShop += SBH_CheckIfCanBuy;
        pcController = GameObject.Find("RangePlayer").GetComponent<PlayerController>();
        pcController.OnObtainedGold += SBH_CheckIfCanBuy;
        nxController = GameObject.Find("NexusB").GetComponent<NexusController>();
        pType = GetComponent<PurchaseableType>();
        StablishPrice();
        myButton = GetComponentInChildren<Button>();
        buttonText = myButton.GetComponentInChildren<TMP_Text>();
        buttonText.text = "Buy: " + price.ToString();
        pc = FindObjectOfType<PlayerController>();
        myButton.onClick.AddListener(OnButtonClick);
        myButton.onClick.AddListener(CheckIfCanBuy);
    }

    public void SBH_CheckIfCanBuy(object sender, EventArgs e)
    {
        if (!pc.GoldCheck(price, false))
        {
            myButton.interactable = false;
        }else
        {
            myButton.interactable = true;
        }
    }

    public void CheckIfCanBuy()
    {
        if (!pc.GoldCheck(price, false))
        {
            myButton.interactable = false;
        }
        else
        {
            myButton.interactable = true;
        }
    }

    private void StablishPrice()
    {
        switch (pType.myType)
        {
            case PurchaseableType.ItemType.Health:
                price = 130;
                break;
            case PurchaseableType.ItemType.Mana:
                price = 110;
                break;
            case PurchaseableType.ItemType.TowerRepair:
                price = 200;
                tower1 = GameObject.Find("Tw1B").GetComponentInChildren<TowerController>();
                tower2 = GameObject.Find("Tw2B").GetComponentInChildren<TowerController>();
                break;
            case PurchaseableType.ItemType.MinionCooldown:
                price = 300;
                break;
            case PurchaseableType.ItemType.Damage:
                price = 150;
                break;
            case PurchaseableType.ItemType.Armor:
                price = 150;
                break;
            case PurchaseableType.ItemType.HealRestore:
                price = 90;
                break;
            case PurchaseableType.ItemType.ManaRestore:
                price = 80;
                break;
            default:
                price = 99999;
                break;
        }
    }

    public void OnButtonClick()
    {
        if (pc.GoldCheck(price, true))
        {
            switch (pType.myType)
            {
                case PurchaseableType.ItemType.Health:
                    pc.AddMaxHealth(200);
                    break;
                case PurchaseableType.ItemType.Mana:
                    pc.AddMaxMana(200);
                    break;
                case PurchaseableType.ItemType.TowerRepair:
                    tower1.TakeDamage(-tower1.MaxHealth);
                    tower2.TakeDamage(-tower2.MaxHealth);
                    break;
                case PurchaseableType.ItemType.MinionCooldown:
                    nxController.spawnRate -= 3;
                    break;
                case PurchaseableType.ItemType.Damage:
                    pc.AddDamage(10);
                    break;
                case PurchaseableType.ItemType.Armor:
                    pc.AddArmor(2);
                    break;
                case PurchaseableType.ItemType.HealRestore:
                    pc.TakeDamage(-pc.MaxHealth);
                    break;
                case PurchaseableType.ItemType.ManaRestore:
                    pc.FillMana(pc.MaxMana);
                    break;
                default:
                    break;
            }
            pcController.UpdateStatsPanel();
        }
    }

}
