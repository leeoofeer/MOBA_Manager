using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurchaseableType : MonoBehaviour
{
    public enum ItemType { Health, Mana, TowerRepair, MinionCooldown, Damage,Armor,HealRestore,ManaRestore }
    public ItemType myType;
}
