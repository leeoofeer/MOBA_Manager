using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerAiming : MonoBehaviour
{
    [SerializeField]
    bool towerBot, towerTop;

    public Transform theTarget;
    public GameObject cannon;
    public GameObject bullet;

    public float fireTimer;
    private bool shootAvailable;    

    void Start()
    {
        InitializeStats();
    }

    private void InitializeStats()
    {
        shootAvailable = true; theTarget = null;
    }

    void Update()
    {
        StartShooting();
    }

    private void StartShooting()
    {
        if (theTarget != null)
        {
            cannon.transform.LookAt(theTarget); cannon.transform.Rotate(0, 0, 0);
            if (shootAvailable) { Shoot(); }
        }
    }

    void Shoot()
    {
        Transform _bullet = Instantiate(bullet.transform, cannon.transform.position, Quaternion.identity);
        _bullet.transform.rotation = cannon.transform.rotation;
        shootAvailable = false;
        StartCoroutine(FireRate());
    }

    IEnumerator FireRate()
    {
        yield return new WaitForSeconds(fireTimer);
        shootAvailable = true;
    }

    private void OnTriggerStay(Collider other)
    {
        if (theTarget == null)
        {
            if (towerBot)
            {
                if (other.gameObject.CompareTag("PlayerT") || other.gameObject.CompareTag("MinionT"))
                { theTarget = other.transform; }
            }
            else if (towerTop)
            {
                if (other.gameObject.CompareTag("PlayerB") || other.gameObject.CompareTag("MinionB"))
                { theTarget = other.transform; }
            }
        }
    }
}
