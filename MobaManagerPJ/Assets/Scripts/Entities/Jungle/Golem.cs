using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Golem : MonoBehaviour
{
    float currentHealth, maxHealth;
    int myReward;
    public float myDamage, damageCD, nextDamage;
    [SerializeField]Image healthBar;
    [SerializeField] GameObject myClone;
    bool imOriginal = true;
    public Transform[] spawnList;


    void Start()
    {
        InitializeStats();
    }

    private void InitializeStats()
    {
        maxHealth = 20;
        currentHealth = maxHealth;
        UpdateHealthBar();
        myDamage = 50;
        myReward = 100;
        damageCD = 2;
        nextDamage = 0;
    }

    public void TakeDamage(float damage, GameObject go)
    {
        currentHealth -= damage;
        UpdateHealthBar();
        if (currentHealth <= 0)
        {            
            PlayerController pCaux = go.GetComponent<PlayerController>();
            pCaux.SumGold(myReward);
            Die();
        }
    }
    private void UpdateHealthBar()
    {
        healthBar.fillAmount = (currentHealth / maxHealth);
    }
    void Die()
    {
        if(imOriginal)SplitInHalf();
        Destroy(gameObject,0.5f);
    }

    private void SplitInHalf()
    {
        StartCoroutine(Split());        
    }

    IEnumerator Split()
    {
        int a = 0;
        Vector3 scaleReduction = new Vector3(1f, 1.5f, 1f);
        while (a != 2)
        {
            GameObject SmallGolem = Instantiate(myClone, spawnList[a].position, Quaternion.identity);
            SmallGolem.transform.localScale -= scaleReduction;
            SmallGolem.GetComponent<Golem>().maxHealth = 500f;
            SmallGolem.GetComponent<Golem>().myDamage = 25f;
            SmallGolem.GetComponent<Golem>().myReward = 25;
            SmallGolem.GetComponent<Golem>().imOriginal = false;
            a++;
            yield return new WaitForSeconds(0.2f);

        }
    }

   
}
