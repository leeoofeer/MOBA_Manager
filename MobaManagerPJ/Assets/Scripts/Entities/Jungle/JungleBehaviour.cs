using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class JungleBehaviour : MonoBehaviour
{
    public NavMeshAgent myAgent;
    public Transform startPos;
    public Vector3 startPosV3;
    public Transform nextTarget =null;
    public float distance;
    float radiusAttack;
    PlayerController myPC;
    Golem myGolem = null;
    Wolfes myWolfes = null;

    private void Awake()
    {
        startPos = this.transform;
        startPosV3 = startPos.position;
    }
    void Start()
    {
        radiusAttack = 7;
        if (name == "BigGolem") { myGolem = GetComponent<Golem>(); }
        else { myWolfes = GetComponent<Wolfes>(); }
    }

    void Update()
    {
        MoveToPlayer();
    }

    private void MoveToPlayer()
    {
        if (nextTarget != null)
        {
            distance = Vector3.Distance(startPosV3, nextTarget.position);
            if (nextTarget.position == startPos.position)  { }
            else if (distance <= radiusAttack)
            {
                myAgent.destination = nextTarget.position;
                myAgent.SetDestination(nextTarget.position);
                Vector3 movementDirection = new Vector3(nextTarget.position.x, 0, nextTarget.position.z);
                movementDirection.Normalize();
                transform.forward = movementDirection;
            }
            else if (distance > radiusAttack)
            {
                myAgent.destination = startPos.position;
                myAgent.SetDestination(startPos.position);
                Vector3 movementDirection = new Vector3(startPos.position.x, 0, startPos.position.z);
                movementDirection.Normalize();
                transform.forward = movementDirection;
            }
        }
        else
        { nextTarget = startPos; }
    }

    private void OnCollisionStay(Collision other)
    {
        if (nextTarget != null)
        {
            if (other.gameObject.CompareTag("PlayerB") || other.gameObject.CompareTag("PlayerB") )
            {
                if (myPC == null) { myPC = other.gameObject.GetComponent<PlayerController>(); }

                if (name == "BigGolem")
                {
                    if (Time.time > myGolem.nextDamage)
                    {
                        myGolem.nextDamage = Time.time + myGolem.damageCD;
                        if (myPC != null) myPC.TakeDamage(myGolem.myDamage);
                    }
                }
                else
                {
                    if (Time.time > myWolfes.nextDamage)
                    {
                        myWolfes.nextDamage = Time.time + myWolfes.damageCD;
                        if (myPC != null) myPC.TakeDamage(myWolfes.myDamage);
                    }
                }                
            }
        }
    }
}
