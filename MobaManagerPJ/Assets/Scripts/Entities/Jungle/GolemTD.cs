using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolemTD : MonoBehaviour
{
    public JungleBehaviour myMoveB;


    void Start()
    {
        myMoveB = GetComponentInParent<JungleBehaviour>();

    }

    private void OnTriggerEnter(Collider other)
    {
        if ((other.gameObject.CompareTag("PlayerB") || other.gameObject.CompareTag("PlayerT") )&& other.gameObject.layer == 4)
        {
            
            UpdateTarget(other);
            Debug.Log("Detecte player: " + other.name);

        }     

    }
    
    private void OnTriggerStay(Collider other)
    {

        if ((other.gameObject.CompareTag("PlayerB") || other.gameObject.CompareTag("PlayerT")) && other.gameObject.layer == 4)
        {

            UpdateTarget(other);
            Debug.Log("Detecte player: " + other.name);

        }

    }
    

    void UpdateTarget(Collider other)
    {
        myMoveB.nextTarget = other.transform;            
        
    }

   
}
