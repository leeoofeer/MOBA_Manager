using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Wolfes : MonoBehaviour
{
    float currentHealth, maxHealth;
    int myReward;
    public float myDamage, damageCD, nextDamage;
    [SerializeField] Image healthBar;

    void Start()
    {
        InitializeStats();
    }

    private void InitializeStats()
    {
        maxHealth = 300;
        currentHealth = maxHealth;
        UpdateHealthBar();
        myDamage = 30;
        myReward = 60;
        damageCD = 1.4f;
        nextDamage = 0;
    }

    public void TakeDamage(float damage, GameObject go)
    {
        currentHealth -= damage;
        UpdateHealthBar();
        if (currentHealth <= 0)
        {
            PlayerController pCaux = go.GetComponent<PlayerController>();
            pCaux.SumGold(myReward);
            Die();
        }
    }
    private void UpdateHealthBar()
    {
        healthBar.fillAmount = (currentHealth / maxHealth);
    }
    void Die()
    {
        
        Destroy(gameObject, 0.5f);

    }
       
  
}
