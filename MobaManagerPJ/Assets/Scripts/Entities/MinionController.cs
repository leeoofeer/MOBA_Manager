using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public abstract class MinionController : MonoBehaviour
{    
    [SerializeField]
    protected Image healthBar;
    protected float maxHealth, currentHealth;
    protected int myDamage, myReward;
    protected PlayerController myPC;
    protected MinionController mController;
    protected NexusController ncController;
    protected TowerController twController;
    protected MinionTD minionTD;
    protected bool haveTarget;
    protected bool teamB, teamT;
    protected float damageCooldown = 1.5f; protected float nextDamage = 0.0f;

    private void Awake()
    {
        InitializeStats();
    }

    private void Start()
    {
        InitializeEnemy();
    }
    
    public abstract void InitializeStats();

    protected virtual void InitializeEnemy()
    {
        currentHealth = maxHealth;
        myPC = null; mController = null;
        minionTD = GetComponentInChildren<MinionTD>();
        StartCoroutine(GetMinionTeam());        
    }

    IEnumerator GetMinionTeam()
    {
        yield return new WaitForSecondsRealtime(3f);
        if (minionTD.minionTeamB)
        {
            teamB = true;
        }
        else if (minionTD.minionTeamT)
        {
            teamT = true;
        }
    }

    public void TakeDamage(float damage, GameObject go)
    {
        currentHealth -= damage;
        UpdateHealthBar();
        if (currentHealth <= 0)
        {
            if (go.CompareTag("PlayerB"))
            {
                PlayerController pCaux = go.GetComponent<PlayerController>();
                pCaux.SumGold(myReward);
            }
            else if (go.CompareTag("PlayerT"))
            {
                PlayerController pCaux = go.GetComponent<PlayerController>();
                pCaux.SumGold(myReward);
            }
            Die();
        }
    }

    public void TakeDamage(float damage)
    {
        currentHealth -= damage;
        UpdateHealthBar();
        if (currentHealth <= 0)  { Die(); }
    }

    private void UpdateHealthBar()
    {
        healthBar.fillAmount = currentHealth / maxHealth;
    }

    void Die()
    {       
        Destroy(gameObject);
    }
}
