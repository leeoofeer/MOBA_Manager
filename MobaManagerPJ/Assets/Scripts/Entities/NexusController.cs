using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class NexusController : MonoBehaviour
{
    public float spawnRate;
    float currentHealth, maxHealth;
    public GameObject goWarrior, goMage;
    public Transform spawnerPosition;
    [SerializeField]
    public Transform enemyNexus;
    Material minionMaterial, minionB, minionT;
    bool isAlive;
    string minionTag;
    bool nexusB, nexusT;
    [SerializeField]
    public Image healthBar;
    public GameObject vcScreen, lsScreen,swScreen;
    [SerializeField]
    public NexusController otherNexus;
    [SerializeField] bool canSpawnWarriors;
    [SerializeField] bool canSpawnMages;


    private void Awake()
    {
        minionB = Resources.Load("Material/MinionB", typeof(Material)) as Material;
        minionT = Resources.Load("Material/MinionT", typeof(Material)) as Material;
    }

    void Start()
    {
        InitializeStats();
        CheckNexus(name);
        SetMinionSpawnData();
        StartCoroutine(SpawnMinions());
        if (nexusT)
        {
            swScreen = vcScreen;
        }
        else if (nexusB)
        {
            swScreen = lsScreen;
        }
    }

    private void InitializeStats()
    {
        maxHealth = 300;
        currentHealth = maxHealth;
        isAlive = true;        
    }

    public void TakeDamage(float damage)
    {
        currentHealth -= damage;
        UpdateHealthBar();
        if (currentHealth <= 0)
        {             
            Die();
        }
    }

    private void Die()
    {

        swScreen.SetActive(true);
        otherNexus.isAlive = false;

        Destroy(gameObject, 0.5f);
        
    }

    private void UpdateHealthBar()
    {
        healthBar.fillAmount = (currentHealth / maxHealth);
    }

    void CheckNexus(string nexusName)
    {
        if (name == "NexusB")
        {
            nexusB = true;
        }

        if (name == "NexusT")
        {
            nexusT = true;
        }
    }

    #region SpawnMinions

    IEnumerator SpawnMinions()
    {
        yield return new WaitForSeconds(1f);

        while (isAlive)
        {
            if(canSpawnWarriors)SpawnWarriors();
            if(canSpawnMages) SpawnMages();
            yield return new WaitForSeconds(spawnRate);
        }
    }
    private void SetMinionSpawnData()
    {
        if (nexusB)
        {
            enemyNexus = GameObject.Find("NexusT").transform;
            minionMaterial = minionB;
            minionTag = "MinionB";
        }
        else if (nexusT)
        {
            enemyNexus = GameObject.Find("NexusB").transform;
            minionMaterial = minionT;
            minionTag = "MinionT";
        }
    }
    void SpawnWarriors()
    {
        StartCoroutine("Spawnwarrior");
    }

    void SpawnMages()
    {
        StartCoroutine("Spawnmages");
    }

    IEnumerator Spawnwarrior()
    {
        int a = 0;
        while (a < 3)
        {
            if (nexusB)
            {
                GameObject newMinion = Instantiate(goWarrior, new Vector3(spawnerPosition.position.x, spawnerPosition.position.y, spawnerPosition.position.z + 2), Quaternion.identity);
                newMinion.GetComponent<MoveBehaviour>().nexusTarget = enemyNexus;
                newMinion.GetComponent<Renderer>().material = minionMaterial;
                newMinion.gameObject.tag = minionTag;
                MinionTD mControl = newMinion.GetComponent<MinionTD>();
                if (mControl != null)
                {
                    if (nexusB)
                    { mControl.minionTeamB = true; }
                    else if (nexusT)
                    { mControl.minionTeamT = true; }
                }
            }
            else if (nexusT)
            {
                GameObject newMinion = Instantiate(goWarrior, new Vector3(spawnerPosition.position.x, spawnerPosition.position.y, spawnerPosition.position.z - 2), Quaternion.identity);
                newMinion.GetComponent<MoveBehaviour>().nexusTarget = enemyNexus;
                newMinion.GetComponent<Renderer>().material = minionMaterial;
                newMinion.gameObject.tag = minionTag;
                MinionTD mControl = newMinion.GetComponent<MinionTD>();
                if (mControl != null)
                {
                    if (nexusB)
                    { mControl.minionTeamB = true; }
                    else if (nexusT)
                    { mControl.minionTeamT = true; }
                }
            }
            
            
            

            yield return new WaitForSeconds(0.8f);
            a++;
        }
    }
    IEnumerator Spawnmages()
    {
        int a = 0;
        yield return new WaitForSeconds(2.5f);
        while (a < 3)
        {            
            GameObject newMinion = Instantiate(goMage, spawnerPosition.position, Quaternion.identity);
            newMinion.GetComponent<MoveBehaviour>().nexusTarget = enemyNexus;
            newMinion.GetComponent<Renderer>().material = minionMaterial;
            newMinion.gameObject.tag = minionTag;
            a++;
            yield return new WaitForSeconds(0.6f);
        }
    }

    #endregion
}
