using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour
{
    public float movementSpeed = 1;

    private void Start()
    {
        StartCoroutine(SelfDestroy());
    }

    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * movementSpeed);
    }        

    IEnumerator SelfDestroy()
    {
        yield return new WaitForSeconds(4f); Destroy(gameObject);
    }
  
}
