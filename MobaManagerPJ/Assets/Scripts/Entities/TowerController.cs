using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerController : MonoBehaviour
{
    private int maxHealth;

    public int MaxHealth
    {
        get { return maxHealth; }
    }


    float currentHealth;
    int myReward;
    [SerializeField] Image healthBar;
    [SerializeField] TowerAiming aimingScript;

    private void InitializeTower()
    {
        maxHealth = 1000;
        currentHealth = maxHealth;        
    }

    void Start()
    {
        InitializeTower();
    }

    public void TakeDamage(float damage, GameObject go)
    {
        currentHealth -= damage;
        UpdateHealthBar();
        if (currentHealth <= 0)
        {
            if (go.CompareTag("PlayerB"))
            {
                PlayerController pCaux = go.GetComponent<PlayerController>();
                pCaux.SumGold(myReward);
            }
            else if (go.CompareTag("PlayerT"))
            {
                PlayerController pCaux = go.GetComponent<PlayerController>();
                pCaux.SumGold(myReward);
            }
            Die();
        }
    }

    public void TakeDamage(float damage)
    {
        currentHealth -= damage;
        UpdateHealthBar();
        if (currentHealth <= 0) { Die(); }
    }
    private void UpdateHealthBar()
    {
        healthBar.fillAmount = (currentHealth / maxHealth);
    }

    void Die()
    {
        aimingScript.enabled = false;
        Destroy(gameObject);
    }
}
