using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletAttack : MonoBehaviour
{
    int myDamage; bool teamT, teamB;
    MinionController mControl;
    PlayerController pControl;
    TowerController twController;
    [SerializeField]
    bool canHitTower;

    void Start()
    {
        mControl = null; pControl = null; twController = null;
        myDamage = 40;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("MinionB") || collision.gameObject.CompareTag("MinionT"))
        {
            if (mControl == null) { mControl = collision.gameObject.GetComponent<MinionController>(); }
            mControl.TakeDamage(myDamage);
            StartCoroutine(SelfDestroyCollision());
        }
        else if (collision.gameObject.CompareTag("PlayerB") || collision.gameObject.CompareTag("PlayerT"))
        {
            if (collision.gameObject.name == "BrokenTower" && canHitTower)
            {
                if (twController == null)
                { twController = collision.gameObject.GetComponent<TowerController>(); }
                twController.TakeDamage(myDamage);
                StartCoroutine(SelfDestroyCollision());
            }
            else if (collision.gameObject.name == "RangePlayer")
            {
                if (pControl == null) { pControl = collision.gameObject.GetComponent<PlayerController>(); pControl.TakeDamage(myDamage); }

                StartCoroutine(SelfDestroyCollision());
            }            
            
        }
    }

    IEnumerator SelfDestroyCollision()
    {
        yield return new WaitForSeconds(0.1f); Destroy(gameObject);
    }
    
}
