using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombController : MonoBehaviour
{
    public float skillDamage = 0;
    void Start()
    {
        StartCoroutine(StartExplosion());
    }   

    IEnumerator StartExplosion()
    {
        yield return new WaitForSecondsRealtime(2f);
        CheckNearBy(skillDamage);
        yield return new WaitForSecondsRealtime(0.1f);
        Destroy(gameObject);
    }

    private void CheckNearBy(float damage)
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, 4f);
        foreach (Collider item in colliders)
        {
            if (item.GetComponent<MinionController>())
            {
                item.GetComponent<MinionController>().TakeDamage(damage);
            }
        }
    }

}
