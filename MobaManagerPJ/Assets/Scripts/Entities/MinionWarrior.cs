using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionWarrior : MinionController
{

    public override void InitializeStats()
    {
        maxHealth = 150;
        myDamage = 10;
        myReward = 25;
        haveTarget = false;      
    }

    private void OnCollisionStay(Collision other)
    {
        if(minionTD.myMoveB.nextTarget != null)
        {
            if (teamB)
            {
                if (other.gameObject.CompareTag("PlayerT"))
                {
                    if (other.gameObject.name == "BrokenTower")
                    {
                        if (twController == null)
                        { twController = other.gameObject.GetComponent<TowerController>(); }

                        if (Time.time > nextDamage)
                        {
                            nextDamage = Time.time + damageCooldown;
                            if (twController != null) twController.TakeDamage(myDamage);
                        }
                    }
                    else
                    {
                        if (myPC == null)
                        { myPC = other.gameObject.GetComponent<PlayerController>(); }

                        if (Time.time > nextDamage)
                        {
                            nextDamage = Time.time + damageCooldown;
                            if (myPC != null) myPC.TakeDamage(myDamage);
                        }
                    }
                }

                if (other.gameObject.CompareTag("MinionT"))
                {
                    if (mController == null)
                    { mController = other.gameObject.GetComponent<MinionController>(); }

                    if (Time.time > nextDamage)
                    { nextDamage = Time.time + damageCooldown; mController.TakeDamage(myDamage,gameObject); }
                }

                if (other.gameObject.CompareTag("NexusT"))
                {
                    if (ncController == null)
                    { ncController = other.gameObject.GetComponentInParent<NexusController>(); }

                    if (Time.time > nextDamage)
                    { nextDamage = Time.time + damageCooldown; ncController.TakeDamage(myDamage); }
                }

            }
            else if (teamT)
            {
                if (other.gameObject.CompareTag("PlayerB"))
                {
                    if (other.gameObject.name == "BrokenTower")
                    {
                        if (twController == null)
                        { twController = other.gameObject.GetComponent<TowerController>(); }

                        if (Time.time > nextDamage)
                        {
                            nextDamage = Time.time + damageCooldown;
                            if (twController != null) twController.TakeDamage(myDamage);
                        }
                    }
                    else
                    {
                        if (myPC == null)
                        { myPC = other.gameObject.GetComponent<PlayerController>(); }

                        if (Time.time > nextDamage)
                        {
                            nextDamage = Time.time + damageCooldown;
                            if (myPC != null) myPC.TakeDamage(myDamage);
                        }
                    }                   
                   
                }

                if (other.gameObject.CompareTag("MinionB"))
                {
                    if (mController == null)
                    { mController = other.gameObject.GetComponent<MinionController>(); }

                    if (Time.time > nextDamage)
                    { nextDamage = Time.time + damageCooldown; mController.TakeDamage(myDamage, gameObject); }
                }

                if (other.gameObject.CompareTag("NexusB"))
                {
                    if (ncController == null)
                    { ncController = other.gameObject.GetComponentInParent<NexusController>(); }

                    if (Time.time > nextDamage)
                    { nextDamage = Time.time + damageCooldown; ncController.TakeDamage(myDamage); }
                }
            }
        }     
    }

}
