using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class MoveBehaviour : MonoBehaviour
{
    public NavMeshAgent myAgent;
    public Transform nexusTarget;
    public Transform nextTarget;

    void Start()
    {
        nextTarget = nexusTarget;
    }

    void Update()
    {        
        if (nextTarget!= null)
        {
            myAgent.SetDestination(nextTarget.position);
            Vector3 movementDirection = new Vector3(nextTarget.position.x, 0, nextTarget.position.z);
            movementDirection.Normalize();
            transform.forward = movementDirection;
        }
        else { nextTarget = nexusTarget; }
    }
}
