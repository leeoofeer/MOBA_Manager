using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionTD : MonoBehaviour
{
    public MoveBehaviour myMoveB;
    public bool minionTeamB, minionTeamT;
    public Material myMat;
    void Start()
    {
        myMoveB = GetComponentInParent<MoveBehaviour>();
        myMat = GetComponentInParent<Renderer>().material;
        if(myMat.name == "MinionB (Instance)") { minionTeamB = true; }
        else if (myMat.name == "MinionT (Instance)") { minionTeamT = true; }
    }  

    private void OnTriggerEnter(Collider other)
    {
        if (minionTeamB)
        {
            if (other.gameObject.CompareTag("PlayerT") || other.gameObject.CompareTag("MinionT"))
            {
                if (other.gameObject.name == "ColliderTower") { AttackTower(other); }
                else { UpdateTarget(other); }

            }
        }else if (minionTeamT)
        {
            if (other.gameObject.CompareTag("PlayerB") || other.gameObject.CompareTag("MinionB"))
            {
                if (other.gameObject.name == "ColliderTower")  { AttackTower(other); }
                else { UpdateTarget(other); }
            }               
        }
    }

    private void AttackTower(Collider other)
    {
        GameObject parent = other.transform.parent.gameObject;
        if (myMoveB.nextTarget != null)
        {
            if (myMoveB.nextTarget == myMoveB.nexusTarget)
            { myMoveB.nextTarget = parent.GetComponent<Transform>(); }
        }
    }

    void UpdateTarget(Collider other)
    {
        if (myMoveB.nextTarget != null)
        {
            if (myMoveB.nextTarget == myMoveB.nexusTarget)
            { myMoveB.nextTarget = other.GetComponentInParent<Transform>(); }
        }
    }

}
