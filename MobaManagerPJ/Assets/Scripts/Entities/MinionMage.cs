using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionMage : MinionController
{
    public Transform theTarget;
    [SerializeField]
    GameObject bullet, cannon;    
    public float fireTimer;
    private bool shootAvailable = true;
    MoveBehaviour myMove;
    [SerializeField]
    Transform nexusTargetT, nexusTargetB;

    public override void InitializeStats()
    {
        maxHealth = 110;
        myDamage = 10;
        myReward = 25;
        myMove = GetComponent<MoveBehaviour>();
        nexusTargetT = GameObject.Find("NexusT").transform;
        nexusTargetB = GameObject.Find("NexusB").transform;
    }

    private void Update()
    {
        StartShooting();        
        theTarget = myMove.nextTarget;
    }

    private void StartShooting()
    {
        if (theTarget != null && theTarget != nexusTargetT && theTarget != nexusTargetB)
        {
            cannon.transform.LookAt(theTarget); cannon.transform.Rotate(0, 0, 0);
            if (shootAvailable) { Shoot(); }
        }
    }

    void Shoot()
    {
        Transform _bullet = Instantiate(bullet.transform, cannon.transform.position, Quaternion.identity);       
        _bullet.transform.rotation = cannon.transform.rotation;
        _bullet.localScale *= 0.5f;
        shootAvailable = false;
        StartCoroutine(FireRate());
    }

    IEnumerator FireRate()
    {
        yield return new WaitForSeconds(fireTimer);
        shootAvailable = true;
    }  
}
